terraform {
  required_version = "~>1.4.2"

  backend "gcs" {
    credentials = "./creds/serviceaccount.json"
    bucket      = "newbucket1234987"
    # Optionally, you might want to add more configurations for the GCS backend
    # For example:
    # prefix = "path/to/your/statefile"
    # region = "us-central1"
  }
}
