provider "google" {
  credentials = file("./creds/serviceaccount.json")
  project     = "steam-circlet-355619"
  region      = "US" # Replace with your desired region
}
