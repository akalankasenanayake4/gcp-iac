resource "google_storage_bucket" "auto-expire" {
  name        = "newbucket1234987_iac"
  location    = "US"
  force_destroy = true
  public_access_prevention = "enforced"

  # Optionally, you can include additional configurations or settings here
  # For example:
  # storage_class = "STANDARD"
  # versioning {
  #   enabled = true
  # }
}
